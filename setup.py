from setuptools import setup, find_packages

version = '0.1'
release = '0.1.1'
setup(
    name='tdatim_category_server',
    version=release,
    description='The Data Times: Category server',
    url='https://gitlab.com/datatimes/tdatim-category-server',
    author='Phil Weir',
    author_email='info@flaxandteal.co.uk',
    license='MIT',
    packages=find_packages('src'),
    package_dir={'': 'src'},
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.6'
    ],
    keywords='category nlp data journalism',
    setup_requires=['pytest-runner'],
    extras_require={
        'examples': []
    },
    install_requires=[
        'flask-restful',
        'gunicorn',
        'gensim',
        'nltk',
        'sortedcontainers'
    ],
    include_package_data=True,
    tests_require=[
        'pytest',
        'pytest-asyncio',
        'mock',
        'asynctest'
    ]
)
