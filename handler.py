import logging
from sortedcontainers import SortedDict
import json
from flask import current_app
from flask_restful import Resource, abort, reqparse
import os

from tdatim_category_server import categories_new as categories

import json

class CategoriesHandler(Resource):
    @classmethod
    def set_category_manager(cls, category_manager):
        cls.category_manager = category_manager

    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('name', type=str, location='json')
        parser.add_argument('entries', type=dict, location='json')
        parser.add_argument('noprepend', type=bool, location='json')
        args = parser.parse_args()

        if args['entries'] and args['name']:
            entries = args['entries']
            name = args['name']
            no_prepend = args['noprepend']
        else:
            parser = reqparse.RequestParser()
            parser.add_argument('name', location='form')
            parser.add_argument('entries', location='form')
            parser.add_argument('noprepend', type=bool, location='form')
            args = parser.parse_args()
            if args['entries'] and args['name']:
                entries = json.loads(args['entries'])
                name = args['name']
                no_prepend = args['noprepend']
            else:
                abort(400, "Entries or name argument missing")

        classifier_bow = SortedDict([])
        for category, labels in entries.items():
            for label, words in labels.items():
                classifier_bow[(category, label)] = words if no_prepend else ([category] + words)
        self.category_manager.add_categories_from_bow(name, classifier_bow)

        return {':'.join(k): v for k, v in classifier_bow.items()}

class Handler(Resource):
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('sentences', type=list, location='json')
        parser.add_argument('categories', type=str, location='json')
        args = parser.parse_args()

        if args['sentences']:
            sentences = args['sentences']
            category_group = args['categories']
        else:
            parser = reqparse.RequestParser()
            parser.add_argument('sentences', location='form')
            parser.add_argument('categories', location='form')
            args = parser.parse_args()
            if args['sentences']:
                sentences = json.loads(args['sentences'])
                category_group = args['categories']
            else:
                abort(400, "Sentence argument missing")

        if category_group:
            results = [self.category_manager.test(sentence, category_group=category_group) for sentence in sentences]
        else:
            results = [self.category_manager.test(sentence) for sentence in sentences]

        return [
            [[float(score), match] for (score, match) in list(result)] for result in results
        ]

    @classmethod
    def preload(cls):
        cc = 'en'
        # TODO: improve this very simplistic approach, when we have more languages available
        if 'TDATIM_LANG' in os.environ:
            cc = os.environ['TDATIM_LANG']

        if cc not in categories.lang_map:
            logging.warn('Could not find default locale in category map')

        cls.category_manager.load(lang=cc)

        return True

    @classmethod
    def get_category_manager(cls):
        return cls.category_manager

    @classmethod
    def set_category_manager(cls, category_manager):
        cls.category_manager = category_manager
