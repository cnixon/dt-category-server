FROM python:3.6
MAINTAINER Phil Weir <phil.weir@flaxandteal.co.uk>
# With acknowledgements to the maintainers of rehabstudio/docker-gunicorn-nginx:
#       Peter McConnell <peter.mcconnell@rehabstudio.com>
#       Patrick Carey

# keep upstart quiet

RUN apt-get update && apt-get install -y \
    build-essential git \
    python3 python3-dev python3-setuptools \
    python3-pip \
    libyaml-dev \
    libssl-dev python3-flask python3-sqlalchemy \
    curl python3-pip && rm -rf /var/cache/apt

ENV DEBIAN_FRONTEND noninteractive

RUN groupadd -r user && useradd -r -g user user
RUN mkdir -p /home/user && chown -R user /home/user

WORKDIR /home/user/

COPY requirements.txt   .
RUN pip3 install -r requirements.txt

COPY index.py           .
COPY handler.py         .
COPY src                .src
COPY setup.py           .
COPY gunicorn_config.py .

RUN cp -R .src src
#ENV PATH=$PATH:/home/user/.local/bin
RUN python3 setup.py develop
USER user

EXPOSE 5000

ENTRYPOINT []

CMD ["gunicorn", "--config", "./gunicorn_config.py", "index:app"]
