#from gensim.models import FastText as ft
import sys
import codecs
import logging
import fasttext
from gensim.models import KeyedVectors
import os
import nltk
import re
from nltk.corpus import stopwords
from sortedcontainers import SortedDict
import numpy as np

THRESHOLD = 0.1

re_sw = re.compile(r'\b\w\b')
re_ws = re.compile(r'\s+')
re_num = re.compile('[^\w\s\']', flags=re.UNICODE)

lang_map = {
    'en': 'english',
    'fr': 'french'
}

DATA_LOCATION = os.environ['TDATIM_DATA'] if 'TDATIM_DATA' in os.environ else '/data'
DATA_CATEGORIES = os.environ['TDATIM_CATEGORIES'] if 'TDATIM_CATEGORIES' in os.environ else f'{DATA_LOCATION}/dtcats.LANG.md'

class CategoryManager:
    _stop_words = None
    _model = None
    _classifier_bow = None
    _topic_vectors = None

    def __init__(self):
        self._categories = {}

    def load(self, lang='en'):
        if not self._stop_words:
            nltk.data.path.append(DATA_LOCATION)
            self._stop_words = stopwords.words(lang_map[lang])
        if not self._model:
            print('LOADING MODEL', file=sys.stderr)
            self._model = fasttext.load_model(f'{DATA_LOCATION}/wiki.{lang}.bin')
            print('LOADED MODEL', file=sys.stderr)
        if 'dtcats' not in self._categories or not self._categories['dtcats']:
            categories_file = DATA_CATEGORIES.replace('LANG', lang)
            classifier_bow = self._load_data_times_categories(categories_file)
            self.add_categories_from_bow('dtcats', classifier_bow)


    def add_categories_from_bow(self, name, classifier_bow):
        topic_vectors = [
            (np.mean(2 * [self._model[k[0]]] + [self._model[w] for w in l], axis=0), [k[0]] + l) for k, l in classifier_bow.items()
        ]
        self._categories[name] = (classifier_bow, topic_vectors)

    def strip_document(self, doc):
        if type(doc) is list:
            doc = ' '.join(doc)

        docs = doc.split(',')
        word_list = []
        for doc in docs:
            doc = doc.replace('\n', ' ').replace('_', ' ').lower()
            doc = re_ws.sub(' ', re_num.sub('', doc)).strip()

            if doc == '':
                return []

            word_list.append([w for w in doc.split(' ') if w not in self._stop_words])

        return word_list

    def _load_data_times_categories(self, dtcat_filename):
        classifier_bow = SortedDict([])

        with codecs.open(dtcat_filename, 'r') as cat_f:
            category = ''
            for line in (l.strip().lower() for l in cat_f.readlines()):
                if not line:
                    continue

                if line.startswith('[') and line.endswith(']'):
                    category = line[1:-1]
                    continue

                if ':' in line:
                    label, words = line.split(':')
                    words = words.split(',')
                else:
                    label, words = (line, [line])

                classifier_bow[(category, label)] = [category] + words
        return classifier_bow

    def test(self, sentence, category_group='dtcats'):
        classifier_bow, topic_vectors = self._categories[category_group]

        clean = self.strip_document(sentence)

        if not clean:
            return []

        tags = set()
        for words in clean:
            if not words:
                continue

            vec = np.mean([self._model[w] for w in words], axis=0)
            result = KeyedVectors.cosine_similarities(vec, [t for t, _ in topic_vectors])
            #result = sorted([(t, model.wv.n_similarity(t, clean)) for t in topics], key=lambda x: x[1], reverse=False)

            top = np.nonzero(result > THRESHOLD)[0]

            tags.update({(result[i], classifier_bow.keys()[i]) for i in top})
        return tags

def make_category_manager():
    category_manager = CategoryManager()

    return category_manager
